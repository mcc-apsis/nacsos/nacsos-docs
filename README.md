# NACSOS documentation

This documentation contains:
* Handbook / Tutorials for end-users
  * via GUI (aka Web UI)
  * via REST API
  * via data-lib / script access
* Technical documentation
  * Description of data model
  * Design decisions
  * Naming and coding conventions

The most recent version is hosted at:  
https://apsis.mcc-berlin.net/nacsos-docs/

Inofficial work-in-progress documentation update:   
https://docs.google.com/document/d/1mahxKP9Ln1GVNgVkB49ACmhh4EDcJ7qvB9co7WPK2J4/edit

GitLab will automatically build the documentation with mkdocs (as described below) and upload it to the APSIS VM via FTP.

Please consult the MKDocs documentation for a reference on the extended markdown features:  
https://squidfunk.github.io/mkdocs-material/reference/

Running the documentation locally:
```bash
# Create environment (optional)
virtualenv venv
# or
python -m venv venv

# Activate environment (optional)
source venv/bin/activate

# install requirements
pip install -r requirements.txt

# run live-server
mkdocs serve --livereload -a localhost:8090

# build locally (into folder "public")
mkdocs build --strict --verbose
```
