# Terms of Service
NACSOS is not a (commercial) product and we are not service providers.

* We cannot offer 24/7 tech support.
* We cannot offer to implement custom features.
* We give no guarantees and we are cannot be held liable for anything the platform does or does not do.

but...

* We are open to invite others to use the platform.
    * Our “fee” is a co-authorship in respective publications (if reasonable; at least cite NACSOS). 
    * If it is in line with our needs, we can implement features. 
    * If resources permit, we can run additional analyses. 
    * We can support the onboarding and introduction to the platform. 
    * You could set up your own instance, it’s open source and “well documented”.
* Data ownership
    * You are the owner of all your data and we will provide all we have if needed (even beyond what you can download via the interface).
    * We do not guarantee the data is safe forever. We do make backups and do our best to not break or loose data.
    * We would also like to use the data, but, of course, you will be included in the process, and we will never publish anything novel with your data.