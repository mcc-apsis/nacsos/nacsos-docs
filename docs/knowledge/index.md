In this section of the documentation, we aim to collect useful resources and best practices on how to conduct good research in general and how the platform supports that process.
NACSOS is built for large-scale systematic maps with multiple collaborators.
Its main purpose is to integrate many data sources (e.g. queries on web of science, scopus, OpenAlex) into a unified dataset, free of duplicates and manage human annotations on the abstract level.
You can manage the annotations by designing annotation schemes, the instructions of how an abstract is to be annotated, assign documents to users for annotation in batches (scopes), and resolve conflicts where users coded an abstract differently to prepare a unified 'gold standard' annotated dataset.

This section contains guides on how to manage your project and some further resources and tools that might help you along the way.

## Why do I need the platform?
Many researchers are familiar with Google Scholar or other academic searches to find literature.
However, for a *systematic* literature review, this is only the starting point as you should consider the complete literature on a topic of interest. 
Given a rigorously evaluated *complete* query on one or several databases, you will now need to go through these results and identify what actually fits the scope of your research projects.
You also need to merge multiple sources (queries from various search databases) and deduplicate.

This is where the platform comes in.
NACSOS helps you to collect literature from various sources, deduplicate the identified literature and sort (we say "annotate") the literature into user-defined categories. 
