# Helpful resources

## Web of Science
For Web of Science we recommend using the [Advanced Search](https://www.webofscience.com/wos/woscc/advanced-search) following their (querying guide)[https://webofscience.help.clarivate.com/en-us/Content/search-operators.html].

## PredicTER
[PredicTER](https://predicter.github.io/) is a tool for estimating how long a review will take to complete. The tool calculates the time requirements for various tasks involved in reviewing evidence, from planning and coordination to quantitative synthesis and reporting.

## Buscar stopping criterion
"BUSCAR: Biased Urns for efficient Stopping Criteria in technology-Assisted Reviews"

When we use machine learning to assist in screening documents for a systematic review, we often identify a majority of the relevant records before we have seen every record. However, if we want to save work, we need to stop early, and we have no way of knowing for sure how many relevant records we might have missed.
If we see a large number of irrelevant records in a row, this is a good sign that the proportion of remaining records that are relevant is low.
Before we use this intuition as a basis for stopping screening, we can consider the number of relevant records we have seen, as well as the number of records we have not yet screened, and calculate the implications of this estimated low prevalence for recall, or the proportion of relevant records we actually identify.

You can play with it in this interactive [online tool](https://mcallaghan.github.io/buscar-app/)

## Other screening/review tools
* https://www.laser.ai/
* EPPI
* Covidence
* PICO
* systematic review accelerator
* https://picoportal.org
* https://skportal.org
* https://ies.epistemonikos.org
* [PAPYRUS-SLR, A VISUALIZATION SOFTWARE FOR SYSTEMATIC LITERATURE REVIEWS](https://papyrus.list.lu/index.html)

