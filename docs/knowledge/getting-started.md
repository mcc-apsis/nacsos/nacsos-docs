There are many protocols and guidelines that help you to conduct a rigorous research project.

## General overview

In any project, you need to decide what data you want to consider for your map or review.
Usually, this is done by designing boolean queries in scientific databases, such as web of science, scopus, or OpenAlex.
Once you have all your data, you then want to *screen* that dataset to identify those articles that are actually
relevant for you.
NACSOS is built for just that and may even help you save time by using machine learning to rank all unseen documents by
relevance based on your prior annotations and stopping early.
You may also annotate several other aspects covered in the articles you screened, such as topic, outcomes, research
methods, etc.
All this is defined by your annotation scheme.

We may be able to help you set these things up on the platform, but you have to

* develop and verify search queries
  * when not using OpenAlex, [download exports in supported formats](../user/import/index.md)
* design and document an annotation scheme
* specify a method/protocol on how to annotate data (which data, in what order, by whom,...)

## Campbell protocol

You may want to consider registering your project with Campbell.
They will guide you through the process of designing a good research protocol.
Otherwise, their registration guides are a helpful starting point for how to conduct a good systematic review or map

* [Submission guide](https://onlinelibrary.wiley.com/page/journal/18911803/homepage/submit-a-proposal)
* [Example protocol by Khanna et al](https://onlinelibrary.wiley.com/doi/full/10.1002/cl2.1424)

## PRISMA

The Preferred Reporting Items for Systematic reviews and Meta-Analyses (PRISMA) has been designed primarily for
systematic reviews of studies that evaluate the effects of health interventions, irrespective of the design of the
included studies.
However, the checklist items are applicable to reports of systematic reviews evaluating other interventions (such as
social or educational interventions), and many items are applicable to systematic reviews with objectives other than
evaluating interventions (such as evaluating aetiology, prevalence, or prognosis).
PRISMA is intended for use in systematic reviews that include synthesis (such as pairwise meta-analysis or other
statistical synthesis methods) or do not include synthesis (for example, because only one eligible study is identified).

* [PRISMA website](https://www.prisma-statement.org/)

## Cochrane protocol

Another very useful guide on how to conduct good reviews, however with a strong focus on health interventions.

* [Cochrane training handbook](https://training.cochrane.org/handbook)