This page hosts some best-practice tips that go beyond any 

## Documentation
It is highly advisable to have a shared collaborative note-taking solution, such as [outline](https://github.com/outline/outline), [notion](https://www.notion.com/), or [evernote](https://evernote.com/).
In there, you should 

* list who is contributing to the project and in which capacity
* document decisions
* keep meeting notes
* write annotation guide/coding guideline with clear descriptions for each label (and choice), inclusion/exclusion rules, maybe with examples
* track choices made while resolving annotations

Many teams try to do this in part with shared documents spread across google, onedrive, or sharepoints.
The multitude of locations and document versions and sharing links regularly leads to confusion and permission issues. 
It is highly recommended to use a single source of truth, keep it up to date, and ensure everyone has seamless access.

## Developing search queries
This section is to be completed, in short, some points:

* Search has to be 'complete'
* Use references you know should be in there
* Use references used in prior work
* Balance precision and recall. A query for 'and' will probably give you all of research, but you have to sift through everything. A too specific query on the other hand might make you miss a lot of relevant literature right from the start