# Representation of human and automatic annotations in the database

Brief primer on main concepts: Scheme, Assignments, Scope/Batch, Annotations, BotAnnotations
Link to [label resolution](../practices/label_resolution.md)

## Annotation schemes

text

## Assignments and annotations

teaser

``` mermaid
erDiagram 
  AnnotationScheme ||--o{ AssignmentScope : has
  AssignmentScope ||--o{ Assignment : has
  Assignment o|--o{ Annotation : has
  AssignmentScope ||--|{ Annotation : has
```

`repeat` indicates the primary, secondary, ternary,... label of a kind (is not tied to any hierarchy)

Conventions: 

* `repeat` starts at 1 always (not 0)
* `repeat` of sublabels (when parent is not null)
  * `repeat` will start at 1 again (i.e. it will not "inherit" the count from the `parent`)
  * this also means, that `UNIQUE('assignment_id', 'key', 'repeat')` does not hold and must also contain `'parent'` 
    (as defined in the schema)


### AssignmentScopes

text

### Assignments

text

## BotAnnotations

teaser


### BotKinds

text

### Metadata and Annotations

text

### Limitations

what annotations are for and what they are NOT for

## Further reading
* [Label resolution](../practices/label_resolution.md)
* [Setting up annotation tasks](../../user/annotation/index.md)