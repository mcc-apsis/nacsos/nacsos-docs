# Schema migration

!!! warning TODO
    This is under construction!

    * why migrations
    * how to handle local migrations (or different states when switching branches during dev)
    * how to handle migration on server
    * how to autogen migrations and verify correctness (up- **and** downgrade)

```bash
# automatically creating up/downgrade script
$ alembic revision --autogenerate -m "Helpful comment"

# apply the migration
$ alembic upgrade head
```