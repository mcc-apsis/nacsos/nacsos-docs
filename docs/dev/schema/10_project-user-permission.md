# Projects, Users, and Permissions

TODO: Create a "best practices" guide in the user section and link to it. 
This should contain guidelines on how to handle different scenarios.


- what's a project
- what's a user
    - how to handle "virtual" users
    - external users
    - ...
- permissions
    - how they work
    - presets