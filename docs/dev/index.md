* `nacsos_data` library
  * Schema/Models
  * CRUD

### using `nacsos_data` in a notebook
the library is async by design. all async functions must be awaited, which is not trivial
on python console. usually used in python notebook anyway, so you just have to turn on autoawait:
https://ipython.readthedocs.io/en/stable/interactive/magics.html#magic-autoawait