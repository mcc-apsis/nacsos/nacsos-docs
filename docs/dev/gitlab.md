* gitlab runner on VM
* hooking runner
* gitlab ci

## Registering a GitLab Runner

=== "1. GitLab CI/CD"
    Get a registration token from GitLab by navigating to your project and open the `Runners` section in `Settings > CI/CD`.
    ![Get info](img/gl-runner.png)
=== "2. Register project"
    We assume you have the [GitLab Runner software installed](https://docs.gitlab.com/runner/install/linux-repository.html).
    ```bash
    # Connect to APSIS VM (or wherever you want to host the runner)
    ssh <user>@srv-mcc-apsis
    # Register project in runner
    sudo gitlab-runner register --url https://gitlab.pik-potsdam.de/ --registration-token <TOKEN>
    ```
=== "3. Registration dialogue"
    Example dialogue with the options taken for the runner registered to the nacsos-core project.
    ```
    Runtime platform                                    arch=amd64 os=linux pid=xxxx revision=xxxx version=xx.x.x
    Running in system-mode.
    
    Enter the GitLab instance URL (for example, https://gitlab.com/):
    [https://gitlab.pik-potsdam.de/]:  ⏎

    Enter the registration token:
    [<TOKEN>]:  ⏎

    Enter a description for the runner:
    [srv-mcc-apsis]: nacsos-core-runner  ⏎

    Enter tags for the runner (comma-separated):
      ⏎

    Enter optional maintenance note for the runner:
      ⏎

    Registering runner... succeeded                     runner=<SUBTOKEN>
    Enter an executor: parallels, shell, virtualbox, kubernetes, custom, docker, docker-ssh, ssh, docker+machine, docker-ssh+machine:
    docker  ⏎

    Enter the default Docker image (for example, ruby:2.7):
    python:3.10-alpine  ⏎

    Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
    
    Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"
    ```