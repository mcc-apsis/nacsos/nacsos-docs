# Welcome to NACSOS

**Quick tour:** [this slide deck](overview_slides.pdf) we created for the WWCS Workshop in Potsdam in Oct 2023.

This documentation has two parts:   
One written for users of the platform and the other to document design decisions and development details for future reference.

!!! quote "Citation"

    If you are using this platform in your work, please consider citing us.
    The most recent publication (pre-print) is available on [arXiv](https://arxiv.org/abs/2405.04621).
    
    ```bibtex
    @article{repke2024nacsos,
      title={NACSOS-nexus: NLP Assisted Classification, Synthesis and Online Screening with New and EXtended Usage Scenarios},
      author={Repke, Tim and Callaghan, Max},
      journal={arXiv preprint arXiv:2405.04621},
      year={2024},
      eprint={2405.04621},
      archivePrefix={arXiv},
      primaryClass={cs.DL},
      url={https://arxiv.org/abs/2405.04621}
    }
    ```

:construction: **This is work in progress!** :construction_site:   
This documentation is not complete.
We are doing our best to keep extending and improving it, but what you are looking for might not yet exist or the documentation may not reflect what the platform is actually doing.
We'll do our best to keep things in sync and up to date.
We greatly appreciate any help in making this page as helpful for everyone as possible.
It's [really easy to edit](https://gitlab.pik-potsdam.de/mcc-apsis/nacsos/nacsos-docs/-/tree/main/docs), just let us know that you'd like to support us.

## Statistics & issues
Currently, the platform hosts ~50 projects, ~150 users, ~340k annotations, and 6M documents (~3.2M academic abstracts, ~1.8M LexisNexis articles, ~800k Tweets).   
_Last updated: 30.08.2024_

We share the platform free of charge.
If you have any issues, [read this](user/issues.md) first.

Login doesn't work and you don't receive a reset link?
[This](user/issues.md) might help you.
