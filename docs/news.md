# News
Here you can find the latest release notes and changes to the platform.   
Technically interested people may be interested in the commit log for 
the [interface](https://gitlab.pik-potsdam.de/mcc-apsis/nacsos/nacsos-web/-/commits/main),
the [data library](https://gitlab.pik-potsdam.de/mcc-apsis/nacsos/nacsos-data/-/commits/main),
or the [backend](https://gitlab.pik-potsdam.de/mcc-apsis/nacsos/nacsos-core/-/commits/main).


## Release notes
### 26.11.2024
* Add [best practices](knowledge/index.md) section to this documentation

### 15.11.2024
* Add priority screening!! Keep track of orderings, train AI, and get useful work estimation plots!
* Add label statistic screen to statistics page
* Add count checker to NQL boxes
* Add numbers to assignment visualiser
* Add "unassign all open for user" to assignment editor
* Remove old assignment configuration in favour of unified priority/random assignments
* Refactor how tables are rendered

### 22.10.2024
* Add item detail view for editing fields, such as title; viewing all variations; and viewing all labels
* Add "assign all to user" to assignment editor
* Add OpenAlex link in AcademicItem component
* Fix precision/recall computation in annotation quality for boolean labels


### 18.10.2024
* Add import revisions to facilitate "living" projects with efficiently tracked query updates!
* Add mutex to prevent parallel imports in a project
* Add feedback on login errors
* Add option to use highlighters anywhere
* Add option for each user to choose active highlighters
* Improve top bar optics
* Improve handling of variations of field values for duplicate items

### 04.09.2024
* Add feature to send platform mails / newsletters to all users (superuser only)
* Improve assignment editor
* Fix import issues; now uses milvus, uses proper transactions, and scales to large imports
* Refactor database/session handling in the backend

### 26.08.2024
* Add new NQL clause to filter abstract length [(see NQL documentation)](user/data-queries.md)
* Add feature to edit assignments (drop all remaining open assignments, fine-grained assignment edit) [(see documentation)](user/annotation/assignments.md)

### 30.07.2024
* Refactor import/deduplication logic for academic items (it now should be faster and scale to larger projects)
* Fix import view to prevent initiating import before first save 
* Drop start/end time of import (redundant with associated "task")
* Add option to remember batch size for annotation tracker
* Add buttons to re-order label sources in annotation tracker

### 24.07.2024
* Improve annotation progress plot (it now updates with the refresh button and when changing between trackers)

### 22.05.2024
* Fix file upload imports
* Fix date display in label overview

### 09.05.2024
* Release NACSOS-nexus pre-print on [arXiv](https://arxiv.org/abs/2405.04621)!

### 29.02.2024
* Add NQL filter to exports (users can now decide which items to download as CSV using NQL)
* Add all item fields to exports (until now, only basic title/abstract information was exported)
* Add "NOT" to NQL (so you can negate entire sub-clauses)
* Add NQL for queries in meta-field
* Fix NQL for SRC clause (did not properly work for LexisNexis)

### 27.02.2024
* Add "WITH" filter in NQL to more easily query for all assignments/annotations for an annotation scheme [(see NQL documentation)](user/data-queries.md)
* Add import deduplication on abstract for research articles [(see new section "Deduplication on the platform")](dev/practices/import_dedup.md)
* Fix assignments with NQL (used to issue "not enough items" warning for LexisNexis projects)
* Fix edge-case error "sentinel values" when updating existing resolutions


### 19.02.2024
* Fix random assignment with NQL filter
* Add [page on mordecai](dev/mordecai.md)

### 18.02.2024
* Add checks to annotation scheme view (tests for unique label keys and valid label keys)
* Clarify and visually highlight notice in annotation scheme view
* Add overview list of bot annotations

### 09.02.2024
* Consolidate annotation schemes, assignment scopes, and resolutions into one view
* Streamline resolution view
* Add more information in annotation progress view
* Add more metrics to annotation quality monitor
* Show user_id in permissions screen
* Facelift sidebar menu
* Facelift annotation scheme edit view

### 30.01.2024
* Imports now in improved sortable list view
* Other minor fixes

### 19.12.2023
* Fix issue in resolution view, where saving causes a missing parent key error
* Show multi-labels in the resolution screen that someone annotated but are dropped in the consolidated label 
* Add a [hidden NQL query parser view](https://apsis.mcc-berlin.net/nacsos/#/parse), which may help learning the language

### 18.12.2023
* New and improved NQL [see here for updated grammar](user/data-queries.md).
* Add NQL to assignment config
* Add "Send password reset link" button to log-in view
* Fix issue in resolution view, where updating a resolution causes "bot_annotation_metadata_id is NULL" error

### 06.12.2023
* Improve list of assignment scopes in the annotations view (now as a sortable table)
* Remove constraints from resolution settings for repeats (now it takes either all repeats or ignores repeats)
* Allow for longer OpenAlex (solr) queries
* Fix issue for Scopus import, where it uploaded all files but only remembered one
* Fix issue for Scopus import, where large files would never really finish uploading
* Fix issue for Scopus import, where files including references were unreadable (but the documentation told users not to include references anyway...)
* Fix dataset stats histogram not working

### 30.11.2023
* Fix issues with saving resolved annotations (vanishing annotations and unclear errors; should now give you a reasonable error message as to why the resolution you are trying to save is not valid)
* Fix issue where password reset link does not redirect to profile page but is stuck on login screen

### 24.11.2023
* Fix issues in annotation interface (vanishing repeated labels, magically changing values, saving last assignment, ...)
* Fix issues with saving resolved annotations (double-saving)

### 01.11.2023
* Add button to send emails to users from the assignment scope config view to inform/remind them that they have open assignments
* Add button in user management view to send welcome mail with initial password
* Add button in user management view to send password reset mail

### 27.10.2023
* Inter-rater-reliability metrics now available via Annotation -> Monitoring  (see [documentation](user/annotation/progress-quality.md))
* Fixed NQL issues with multiple clauses

### 13.10.2023
* Buscar stopping criterion now available via Annotation -> Progress (see [documentation](user/annotation/progress-quality.md))
* Items can now be shown in columns for better readability
* Fixed issues with saving resolved annotations
* Add LexisNexis support

### 29.08.2023
Ordering of assignments was sometimes inconsistent. 
The ordering is now fixed and the progress indicators now follow the proper order.

### 25.08.2023
Keyboard bindings: Users can now annotate faster and more comfortably by just using the keyboard (in most cases).
For further details, check out [the documentation](user/annotation/annotation.md).

### 18.08.2023
Indicators in the progress bar of the assignment view now reveal an identifying number users may refer to and take notes on that might help them when resolving annotaiton conflicts. 

### 17.08.2023
Fundamentally refactored handling of imports.
We started off with trying to generalise import helpers to allow for wide range of supported formats.
This added a lot of complexity and was confusing for users.
In favour of simplicity, we now have many duplicated code fragments but a more intuitive interface and adding new formats might become easier in the future.

### 10.08.2023
Introducing the [NACSOS Query Language (NQL)](user/data-queries.md).
The NQL is an intuitive but powerful way to describe which articles of a project's dataset a user likes to select.
At this point, most of the basic features of NQL are supported by the backend and you can use it with the `nacsos_data` library.
The webinterface exposes NQL capabilities in the search bar in the dataset view.
In the future, we are planning to replace most or all filters on the platform with a unified NQL entry tool that helps you find the IDs you may want to refer to.
This allows users to be very specific about what documents they want to sample from for the next batch of assignments or for exporting.

### 07.08.2023
We now integrated our self-hosted OpenAlex database.
Users with the correct permission are now able to search mirror in Solr directly on the platform to develop their query and check for possible alias terms.
The documentation now has [an extra page](user/import/openalex.md) on that.
Given a query, we also support direct import from OpenAlex.
Please use this responsibly, as wrong settings may cause you to import millions of documents into the NACSOS database.

### Before August 2023
Everything else you see.
