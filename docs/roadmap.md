# Roadmap
Some known issues and bigger features that we are planning to work on:

* Generate prioritised label assignments using basic machine learning models directly on the platform
* Stored searches
* Support for RIS, Zotero, Endnote
* Make NQL available everywhere for a unified way to select data for assignments, export, ...
* Implement interactive NQL parser that suggests IDs and provides hints
* Make deduplication and import overlap more transparent

*Last updated: 22.10.2024*