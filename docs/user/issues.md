# Problems with the platform
We do our best to test everything, but it sometimes happens that something goes wrong.
This page contains guides on how you can check if something was just a fluke or an actual problem with the platform.
Sure, there should never be errors, but building an extremely robust web interface is very complex, we rather spend time developing helpful features that work 98% of the time.
Having said that, please do not hesitate to report even small issues that you come across, over time, we should fix those anyway.
This also applies to suggestions on how to improve the flow of an existing functionality or what to add.
Note, that there is already a [list of plans](../roadmap.md) and the [news page](../news.md) with updates. 

??? tip "Preventing issues"
    The best way to prevent errors is to use the platform patiently.

    The platform will indicate that it is working in the background.
    When you click buttons multiple times, this is a good way to cause issues.

    Always wait for the page to fully load and wait a moment, even when it seems nothing is happening.
    Sometimes, some internal process is working (which blocks the rendering, so we can't even tell you to wait) and you can continue in just a few seconds.

## Something went wrong, what can I do
* Is it in the list of known issues?
* If it makes sense, take screenshots of the entire page (e.g. when you spent hours on resolving labels). In this way, you can at least quickly reproduce results later.
* Refresh the page, ideally "hard-reload" and delete cache
* Try to do exactly what you just did
* Still there, then probably something is wrong...
* Let someone know (see below)!

## I can't log in!
Please make sure that you are using the correct username.
This is not your email address.
Usually the username is all lowercase following the format `firstname.lastname`.
For example, **"Biene Maja" with the email address "maja@himmelsvolk.eu" probably has the username "biene.maja".**

When she logs in, the server will create an _authentication token_ that the browser has to remember and send with every future request to prove it's her
In this way, the password always stays safe and this token is now used instead after the login.
When she switches browsers or devices, she needs to log in again and get a new token.
The old token in the other browser is now invalid.

![Password extend](./img/replenish.png){ width=400  loading=lazy }

Also, any token is not valid forever and is usually invalid after two weeks.
The platform will typically warn you about this so you can extend it (see figure above; click on replenish, then extend).
Of course, you can simply log in again when things stop working.

![Password reset](./img/login.png){ width=400  loading=lazy }

When Biene Maja forgets her password, the [computer says no](https://www.youtube.com/watch?v=x0YGZPycMEU) but there's not way to [turn it off and on again](https://www.youtube.com/watch?v=5UT8RkSmN4k).
So, she clicks the "reset password" button on the login page.
This will create an authentication token on the server and send it to her email address.
This is only valid for a short time to make sure it cannot be misused by others. 
When she clicks the link, she's automatically logged in and forwarded to the "edit user profile" page and immediately sets a new password and updates it in her password manager (she's a great fan of free and open software like [vaultwarden](https://github.com/dani-garcia/vaultwarden) or [keepass](https://keepass.info/)).
To verify everything worked well, she'll log out and log in with the new credentials.
This is necessary anyway to have the browser remember a proper, longer-lasting authentication token.

Password reset emails are usually sent immediately, but may also take time to arrive in your inbox or your spam folder.
Be patient and verify that you used the correct username.
The platform will never give any feedback if the username you tried even exists (otherwise people could just guess random usernames through multiple tries).

Please note, that there can always only be one authentication token per user at a time.
This means, that logging in somewhere typically (in some cases it will send the existing one and only extend its validity) invalidates a previous token (e.g. on another device).
Also, when you click the password reset link (or an administrator does it for your), the previous token is invalidated.

## Newsletter / User profile
We send irregular messages to platform users, for example when we introduce changes and new features to the platform or to let you know when we are experiencing problems.
You can of course stay informed about updates [in this documentation](../news.md) or even subscribe to [the RSS commit feed](https://gitlab.pik-potsdam.de/mcc-apsis/nacsos/nacsos-docs/-/commits/main?feed_token=glft-c34147654869dc6f0551e014d0190411498bd1aec1fa42c24077e1be934643dd-914&format=atom) of this documentation. 
This is an opt-in feature and you can always change your settings.
As shown in the screenshot below, open the menu from the top-right corner and click on "Edit profile" (your menu may look different).
On the profile editing page, check the "I want to receive..." box and click save.
There is no "signup confirmation mail".

![User profile](./img/newsletter.png){ width=400  loading=lazy }

## Known issues
* When you save a label resolution, you may get an error mentioning "There appears to be no parent for this label". This is actually by design. In the background, the platform checks, whether the resolution (at least to an extent) is in line with the annotation scheme. If you have nested labels and deleted the parent, you also have to clear all child elements. The interface should indicate most potential issues with an orange background.
* You wrote a query for something and receive an "InvalidNQLError". In this case, the query might have been grammatically okay, but does not make sense for this particular project or situation.
* Not confirmed (yet), but there might be side-effects if you use multiple tabs and have different projects open in each tab.
* The developer uses the Chrome browser. Unfortunately, the browser, operating system, device, and many other circumstances can lead to different outcomes. When in doubt, use the most recent version of Chrome.
* Site is not reachable: This might be that the server is updating or restarting or the network to our servers broke.
* Seemingly random errors (especially when the page has been loading for a while): This is our of our hands, unfortunately. The server that bridges our server with the internet sometimes decides to cut the connection mid-way and respond with something else. This something else will confuse the interface and maybe it holds, sometimes it breaks slightly, sometimes badly. In this case, wait a moment (you might be temporarily blocked) and reload the page.

## How to report a bug
Statements such as "XY doesn't work" or "I can't do XY" do not provide enough context to identify an issue.
In general, the platform is fairly stable.
Errors that do occur are therefore very specific to what you are trying to do, so the only way for a developer to effectively investigate an issue is to provide as many details as possible.
In this way, you also show respect to the people working hard to provide a useful tool for you instead of leaving them spending a lot of time guessing what and where the issue you found might be. 

Report must include:

* Always include name and ID of the project
* Always include name and ID of the annotation scheme, assignment scope, resolution, ... or whatever your issue relates to
* Describe what you did as detailled as possible
* Describe what happened (and what you assumed should happen)

Additional information is highly appreciated:

* Save the server communication
    * Open the developer tools of your browser (usually F12)
    * Open the network tab (see below, might look different in your browser)
    * Note, this might be empty; depending on the settings, the traffic is not recorded at all times. In this case, you need to reproduce the error.
    * Look for any red entries (status code not 200)
    * Right click on that entry and go to copy -> copy as curl (copy this into a text file)
    * Right click on that entry and go to copy -> copy request headers (copy this into a text file, can be the same)
    * Right click on that entry and go to copy -> copy response (copy this into a text file, can be the same)
    * Right click on that entry and go to copy -> copy stack trace (copy this into a text file, can be the same)
* Save the console output
    * Open the developer tools of your browser (usually F12)
    * Open the console tab
    * Expand all entries (at least the last few) by clicking the caret on the left (one or two levels is fine, see below)
    * Copy the logs (you can also save each entry, but this can be tedious and produce large files); easiest might be to take screenshots
* In some cases, it helps to screenshots of (the issue in) the interface

![Network tab in developer console](./img/issues-network.png){ width=400  loading=lazy }
![Console tab in developer console](./img/issues-console-1.png){ width=400  loading=lazy }
![Console tab (expanded) in developer console](./img/issues-console-2.png){ width=400  loading=lazy }

