# NACSOS User Guide

* Background (prefix `bg-`)
    * High-level overview of basic concepts
    * High-level overview of "under the hood" things
* Basic Web UI introduction (prefix `web-`)
    * user guides for common tasks
* Data access (prefix `data-`)
    * Introduction to REST API (+ how to use and when)
    * Accessing data via script
