# Importing Web of Science
There is no direct import from Web of Science on the new platform, yet.
For the vast majority of projects, the time spent on downloading a few files far outweighs the time spent on constantly fixing and maintaining a scraper. 

In the result list, click "Export" > "Plain text file":   
![Click "export" > "plain text" at top of result list](img/wos_01.png){ width=400  loading=lazy }   

In the modal that pops up, you can download up to 1000 records at a time (keep adjusting start/end).
Make sure to select "Full Record"!   
![Web of Science export settings](./img/wos_02.png){ width=400  loading=lazy }

It seems like WoS changed the export options in Q3 2024.
If you do not get the "full record" option, choose the custom export with the following fields:
![Web of Science export settings](./img/wos_03.png){ width=400  loading=lazy }

Once you downloaded the "plain text file" with "Full Record" exports, you can upload those to the platform.
You can either use the script below or the web interface.

![NACSOS upload interface](./img/import_interface.png){ width=400  loading=lazy }

1. Provide a name and description, click save
2. Select the exported file, click upload and then save
3. Click "initiate import"

At this point, there is no option to bulk-upload multiple files (unless you use a script).

On the bottom of the page you can track the progress, the stats can be refreshed repeatedly to see how many items are already imported.

??? "Script for importing Web of Science"
    ```python
    import asyncio
    
    from nacsos_data.db import get_engine_async
    from nacsos_data.util.academic.wos import read_wos_file
    from nacsos_data.util.academic.importer import import_academic_items
    
    
    async def main() -> None:
        # The project to import data to
        PROJECT_ID = '???'
        # The user this import will be attached to
        USER_ID = '???'
        
        # Name of the import
        IMPORT_NAME = '???'
        # Description of the import (e.g. the query used and when it was downloaded)
        IMPORT_DESC = '???'
    
        db_engine = get_engine_async(conf_file='/path/to/remote.env')
        scopus_works = list(read_wos_file('/path/to/wos.txt',
                                          project_id=PROJECT_ID))
        await import_academic_items(items=scopus_works,
                                    db_engine=db_engine,
                                    project_id=PROJECT_ID,
                                    import_name=IMPORT_NAME,
                                    description=IMPORT_DESC,
                                    # you can set this if you rather want to 
                                    # add new documents to an existing import
                                    import_id=None,
                                    user_id=USER_ID,
                                    # Set this to True to simulate the import without actually doing it
                                    dry_run=False,
                                    # Set these to False if you don't want to overwrite values of duplicates
                                    trust_new_authors=True,
                                    trust_new_keywords=True)
    
    
    if __name__ == '__main__':
        asyncio.run(main())
    ```